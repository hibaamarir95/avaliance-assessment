<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Session;
use Excel;
use App\Imports\CsvImport;

class AgentController extends BaseController
{
    public function assignLeadsToAgents(Request $request){
		
		if($request->hasFile('agents') && $request->hasFile('leads')){
            
			$path = $request->file('agents')->getRealPath();
			$agents =Excel::toArray(new CsvImport, $request->file('agents')); 
			$leads =Excel::toArray(new CsvImport, $request->file('leads')); 
			$result = [];
			$current_leads_row = 1;
			$total_leads = count($leads[0])-1;
			while($current_leads_row < $total_leads){
				
				foreach ($agents[0] as $k => $agent) {
					
					if($agent[2] == "Available" ){
						$weight = $agent[3];
						$current_weight = 0;
						if(isset($results[$k])){
							$i= count($results[$k]["leads"]);
						}else{
							$i = 0;
							$results[$k]["name"] = $agent[1];
						}
						
						
						
						
						while($current_weight < $weight && $current_leads_row <= $total_leads){
							
							$results[$k]["leads"][$i]["id"] = $leads[0][$current_leads_row][0];
							$results[$k]["leads"][$i]["name"] = $leads[0][$current_leads_row][2];
							$results[$k]["leads"][$i]["email"] = $leads[0][$current_leads_row][1];
							$i++;
							$current_leads_row++;
							$current_weight++;
							
						}
					}
					
				}
				
			}
			 return view('result', compact('results'));
			
		}
	}
}
