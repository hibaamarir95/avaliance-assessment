<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Styles -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
	
	<div class="container">
		<h1 class="text-center"> List of leads with the assigned agent</h1>
		  <div class="row">
			  
			  <div class="col-sm-12 col-md-10 col-md-offset-1">
			  @foreach($results as $result)
				<h3><strong>Agent name : </strong>{{$result["name"]}}</h3>
				<table class="table">
					  <thead>
						<tr>
						  <th scope="col">#</th>
						  <th scope="col">Email</th>
						  <th scope="col">Name </th>
						</tr>
					  </thead>
					  <tbody>
					  @foreach($result["leads"] as $lead)
						<tr>
						  <th scope="row">{{$lead["id"]}}</th>
						  <td>{{$lead["email"]}}</td>
						  <td>{{$lead["name"]}}</td>
						</tr>
					   @endforeach
					  </tbody>
				</table>  
			   @endforeach	
			  </div>    
		  </div>
		</div>
   

</body>
</html>


