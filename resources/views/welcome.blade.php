<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Styles -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
	
	<div class="container">
		  <div class="row">
			<div class="Absolute-Center is-Responsive">
			  
			  <div class="col-sm-12 col-md-10 col-md-offset-1">
				<form action="/assign" id="assign" method="post" enctype="multipart/form-data">
					@csrf
				  <div class="form-group">
					<label for="agents">Agents</label>
					<input type="file" class="form-control-file" name="agents" id="agents">
				  </div>
				  <div class="form-group">
					<label for="leads">Leads</label>
					<input type="file" class="form-control-file" name="leads" id="leads">
				  </div>
				  <div class="form-group">
					<button type="submit" class="btn btn-primary btn-block">Assign</button>
				  </div>
				</form>        
			  </div>  
			</div>    
		  </div>
		</div>
   

</body>
</html>
